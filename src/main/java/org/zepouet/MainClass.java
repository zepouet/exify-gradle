package org.zepouet;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {
    public static void main(String[] args) {
        try (Stream<Path> walk = Files.walk(Paths.get(args[0]))) {

            List<String> result = walk.filter(Files::isRegularFile)
                    .filter(isValid())
                    .map(x -> x.toString())
                    .collect(Collectors.toList());

            result.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static Predicate<Path> isValid() {
        return path -> path.toString().endsWith("java");
    }
}
